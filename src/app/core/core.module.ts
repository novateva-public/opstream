import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { NgTimePastPipeModule } from 'ng-time-past-pipe';
import { UrlPipe } from './pipes/url.pipe';
import { RouterModule } from '@angular/router';
import { CleanTextPipe } from './pipes/clean-text.pipe';

@NgModule({
  declarations: [HeaderComponent, FooterComponent, UrlPipe, CleanTextPipe],
  imports: [CommonModule, HttpClientModule, NgTimePastPipeModule],
  exports: [
    HeaderComponent,
    FooterComponent,
    HttpClientModule,
    NgTimePastPipeModule,
    UrlPipe,
    RouterModule,
    CommonModule,
    CleanTextPipe,
  ],
})
export class CoreModule {}
