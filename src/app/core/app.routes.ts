import { Routes } from '@angular/router';

export const AppRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('../news/news.module').then((m) => m.NewsModule),
  },
];
