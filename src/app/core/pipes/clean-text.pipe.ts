import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cleanText',
})
export class CleanTextPipe implements PipeTransform {
  transform(value: String | undefined, ...args: unknown[]): unknown {
    if (!value) return '';
    const doc = new DOMParser().parseFromString(
      value.replaceAll('\n', '').replaceAll('<p>', '\n\n') as string,
      'text/html'
    );
    return doc.documentElement.textContent;
  }
}
