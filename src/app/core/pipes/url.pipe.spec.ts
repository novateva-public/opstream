import { UrlPipe } from './url.pipe';

describe('UrlPipe', () => {
  it('create an instance', () => {
    const pipe = new UrlPipe();
    expect(pipe).toBeTruthy();
  });

  it('should return domain', () => {
    const pipe = new UrlPipe();
    expect(pipe.transform('https://example.com')).toEqual('example.com');
    expect(pipe.transform('www.google.com')).toEqual('');
  });
});
