import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'url',
})
export class UrlPipe implements PipeTransform {
  transform(value: String, ...args: unknown[]): String {
    try {
      let domain = new URL(value as string);

      return domain.hostname;
    } catch (error) {
      return '';
    }
  }
}
