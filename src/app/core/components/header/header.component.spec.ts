import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should have link with the text', () => {
    const elements = [
      '',
      'Hacker News',
      'new',
      'past',
      'comments',
      'ask',
      'show',
      'jobs',
      'submit',
      'login',
    ];
    const fixture = TestBed.createComponent(HeaderComponent);
    const compiled = fixture.nativeElement as HTMLElement;
    const htmlElements = compiled.querySelectorAll('.item');
    htmlElements.forEach((element, index) => {
      expect(element.childNodes[0].textContent).toEqual(elements[index]);
    });
  });

  it('Should have image with link ', () => {
    const fixture = TestBed.createComponent(HeaderComponent);
    const compiled = fixture.nativeElement as HTMLElement;
    const htmlElement = compiled.querySelector('.item');
    const image = htmlElement?.childNodes[0].childNodes[0] as HTMLImageElement;
    expect(image.src).toContain('assets/y18.gif');
  });
});
