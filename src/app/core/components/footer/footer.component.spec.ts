import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StoryMockServiceService } from '@core/mocks/story-mock-service.service';
import { StoryService } from '@core/service/story.service';

import { FooterComponent } from './footer.component';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FooterComponent],
      providers: [{ provide: StoryService, useClass: StoryMockServiceService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should have link with the text', () => {
    const elements = [
      'Guidelines',
      'FAQ',
      'Lists',
      'API',
      'Security',
      'Legal',
      'Apply to YC',
      'Contact',
    ];
    const fixture = TestBed.createComponent(FooterComponent);
    const compiled = fixture.nativeElement as HTMLElement;
    const htmlElements = compiled.querySelectorAll('.item');
    htmlElements.forEach((element, index) => {
      expect(element.childNodes[0].textContent).toEqual(elements[index]);
    });
  });
});
