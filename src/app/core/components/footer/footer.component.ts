import { Component, OnInit } from '@angular/core';
import { StoryService } from '@core/service/story.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass'],
})
export class FooterComponent implements OnInit {
  constructor(private storyService: StoryService) {}

  ngOnInit(): void {}

  onChangeText(event: Event) {
    this.storyService.changeFilter(
      (event.target as HTMLInputElement)?.value || ''
    );
  }
}
