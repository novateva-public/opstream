import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Story } from '@shared/models/story/story.interface';
import { IStoryService } from '@shared/models/story/story.interface.service';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StoryService implements IStoryService {
  private stringFilter: String = '';
  nameChange: Subject<String> = new Subject<String>();

  constructor(private http: HttpClient) {}

  getTopStories(): Observable<Number[]> {
    return this.http.get<Array<Number>>(
      'https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty'
    );
  }

  getStory(id: Number) {
    return this.http.get<Story>(
      `https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`
    );
  }

  changeFilter(string: String) {
    this.nameChange.next(string);
    this.stringFilter = string;
  }
}
