import { HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { CoreModule } from '@core/core.module';
import { Story } from '@shared/models/story/story.interface';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

import { StoryService } from './story.service';

describe('StoryServiceService', () => {
  let service: StoryService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(StoryService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return expected story (HttpClient called once)', () => {
    const expectedStory: Story = {
      by: 'dhouston',
      descendants: 71,
      id: 8863,
      kids: [
        9224, 8917, 8952, 8958, 8884, 8887, 8869, 8873, 8940, 8908, 9005, 9671,
        9067, 9055, 8865, 8881, 8872, 8955, 10403, 8903, 8928, 9125, 8998, 8901,
        8902, 8907, 8894, 8870, 8878, 8980, 8934, 8943, 8876,
      ],
      score: 104,
      time: 1175714200,
      title: 'My YC app: Dropbox - Throw away your USB drive',
      type: 'story',
      url: 'http://www.getdropbox.com/u/2/screencast.html',
    };

    service.getStory(1).subscribe((story) => {
      expect(story).withContext('expected heroes').toEqual(expectedStory);
    });
    const req = httpMock.expectOne(
      `https://hacker-news.firebaseio.com/v0/item/1.json?print=pretty`
    );
    req.flush(expectedStory);
  });
});
