import { TestBed } from '@angular/core/testing';

import { StoryMockServiceService } from './story-mock-service.service';

describe('StoryMockServiceService', () => {
  let service: StoryMockServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StoryMockServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
