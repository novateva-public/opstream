import { Injectable } from '@angular/core';
import { Story } from '@shared/models/story/story.interface';
import { IStoryService } from '@shared/models/story/story.interface.service';
import { of, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StoryMockServiceService implements IStoryService {
  private stringFilter: String = '';
  nameChange: Subject<String> = new Subject<String>();
  mockStories: Story[] = [
    {
      by: 'dhouston',
      descendants: 71,
      id: 8863,
      kids: [2],
      score: 104,
      time: 1175714200,
      title: 'My YC app: Dropbox - Throw away your USB drive',
      type: 'story',
      url: 'http://www.getdropbox.com/u/2/screencast.html',
    },
    {
      by: 'Cthulhu_',
      id: 2,
      kids: [3],
      text: 'I mean charities could open up crypto accounts; since it&#x27;s a public ledger, the ransomware operators would be able to see the transaction.',
      time: 1653649807,
      type: 'comment',
      descendants: 0,
      score: 0,
      title: '',
      url: '',
    },
    {
      by: 'Cthulhu_',
      id: 3,
      kids: [],
      text: 'I mean charities could open up crypto accounts; since it&#x27;s a public ledger, the ransomware operators would be able to see the transaction.',
      time: 1653649807,
      type: 'comment',
      descendants: 0,
      score: 0,
      title: '',
      url: '',
    },
  ];

  private mockTopStory: Array<Number> = [8863];

  constructor() {}

  getTopStories() {
    return of(this.mockTopStory);
  }

  // returns observable that resolves to ID of new Story
  getStory(id: Number) {
    return of(this.mockStories.find((item) => item.id === id));
  }

  changeFilter(string: String) {
    this.nameChange.next(string);
    this.stringFilter = string;
  }
}
