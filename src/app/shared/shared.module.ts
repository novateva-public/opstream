import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as fromComponents from './components';
import { CoreModule } from '@core/core.module';

@NgModule({
  declarations: [...fromComponents.components],
  imports: [CommonModule, CoreModule],
  exports: [...fromComponents.components],
})
export class SharedModule {}
