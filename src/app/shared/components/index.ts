import { ListComponent } from './list/list.component';
import { ItemComponent } from './list/item/item.component';
import { CreateFormComponent } from './comments/create-form/create-form.component';
import { CommentComponent } from './comments/comment/comment.component';
export const components: any[] = [
  ListComponent,
  ItemComponent,
  CreateFormComponent,
  CommentComponent,
];
export * from './list/list.component';
export * from './list/item/item.component';
