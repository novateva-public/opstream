import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StoryMockServiceService } from '@core/mocks/story-mock-service.service';
import { environment } from '../../../../environments/environment.testing';
import { ListComponent } from './list.component';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListComponent],
      providers: [{ useValue: environment }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have n elements', () => {
    const fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    component.items = new StoryMockServiceService().mockStories;
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    const htmlElement = compiled.querySelectorAll('app-item');
    expect(htmlElement.length).toBe(component.items.length);
  });
});
