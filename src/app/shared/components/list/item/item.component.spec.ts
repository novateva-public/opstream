import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@core/core.module';
import { StoryMockServiceService } from '@core/mocks/story-mock-service.service';

import { ItemComponent } from './item.component';

describe('ItemComponent', () => {
  let component: ItemComponent;
  let fixture: ComponentFixture<ItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ItemComponent],
      imports: [CoreModule, RouterTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have content', () => {
    const fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;
    const item = new StoryMockServiceService().mockStories[0];
    component.item = item;
    fixture.detectChanges();

    const compiled = fixture.nativeElement as HTMLElement;
    const title = compiled.querySelector('.news-title-link')
      ?.innerHTML as String;
    const score = compiled.querySelector('.score')?.innerHTML as String;
    const link = (compiled.querySelector('.site-link') as HTMLLinkElement)
      .href as String;
    const comments = compiled.querySelector('.comments')?.innerHTML as String;
    expect(title.trim()).toEqual(item.title.trim());
    expect(score).toContain(String(item.score));
    expect(link).toEqual(item.url);
    expect(comments).toContain(String(item.descendants));
  });
});
