import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Story } from '@shared/models/story/story.interface';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.sass'],
})
export class ItemComponent implements OnInit {
  @Input() item: Story = {} as Story;
  @Input() index: number = 0;
  @Input() isItemDetail: boolean = false;
  constructor(private router: Router) {}

  ngOnInit(): void {}

  toDate(timestamp: Number): Date {
    return new Date(Number(timestamp) * 1000);
  }
  goToStory(): void {
    this.router.navigate([`item/${this.item.id}`]);
  }
}
