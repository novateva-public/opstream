import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Story } from '@shared/models/story/story.interface';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass'],
})
export class ListComponent implements OnInit {
  @Input() items: Array<Story> = [];
  constructor() {}

  ngOnInit(): void {}
}
