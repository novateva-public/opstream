import { Component, Input, OnInit } from '@angular/core';
import { StoryService } from '@core/service/story.service';
import { Story } from '@shared/models/story/story.interface';
import { forkJoin, map, of, switchMap } from 'rxjs';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.sass'],
})
export class CommentComponent implements OnInit {
  @Input() item: Story = {} as Story;
  @Input() level: number = 0;
  hide: boolean = false;
  comments: Story[] = [];
  constructor(private storyService: StoryService) {}

  ngOnInit(): void {
    if (this.item.kids?.length)
      of(this.item.kids)
        .pipe(
          map((ids) => ids.map((id) => this.storyService.getStory(id))),
          switchMap((storiesData$) => forkJoin(...storiesData$))
        )
        .subscribe((comments) => (this.comments = comments));
  }

  toDate(timestamp: Number): Date {
    return new Date(Number(timestamp) * 1000);
  }

  switchHide() {
    this.hide = !this.hide;
  }
}
