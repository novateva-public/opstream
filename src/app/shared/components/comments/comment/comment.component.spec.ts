import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CoreModule } from '@core/core.module';
import { StoryMockServiceService } from '@core/mocks/story-mock-service.service';
import { CleanTextPipe } from '@core/pipes/clean-text.pipe';
import { StoryService } from '@core/service/story.service';

import { CommentComponent } from './comment.component';

describe('CommentComponent', () => {
  let component: CommentComponent;
  let fixture: ComponentFixture<CommentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CommentComponent],
      imports: [CoreModule],
      providers: [{ provide: StoryService, useClass: StoryMockServiceService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render comment with response', () => {
    const fixture = TestBed.createComponent(CommentComponent);
    component = fixture.componentInstance;
    component.item = new StoryMockServiceService().mockStories[1];
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    const htmlElement = compiled.querySelectorAll('app-comment');
    expect(htmlElement.length).toBe(component.item.kids.length);
  });

  it('should render comment information', () => {
    const fixture = TestBed.createComponent(CommentComponent);
    component = fixture.componentInstance;
    component.item = new StoryMockServiceService().mockStories[2];
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    const cleanTextPipe = new CleanTextPipe();
    const owner = compiled.querySelector('.owner')?.innerHTML;
    const text = compiled.querySelector('.text')?.innerHTML;
    expect(owner).toContain(component.item.by);
    expect(text).toContain(cleanTextPipe.transform(component.item.text));
  });
});
