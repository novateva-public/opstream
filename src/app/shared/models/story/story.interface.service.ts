import { Observable } from 'rxjs';
import { Story } from './story.interface';

export interface IStoryService {
  // returns observable that resolves to list of todos
  getTopStories(): Observable<Array<Number>>;

  // returns observable that resolves to ID of new Story
  getStory(id: Number): Observable<Story | null | undefined>;
}
