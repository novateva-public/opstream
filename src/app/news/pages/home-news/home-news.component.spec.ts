import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CoreModule } from '@core/core.module';
import { StoryMockServiceService } from '@core/mocks/story-mock-service.service';
import { StoryService } from '@core/service/story.service';

import { HomeNewsComponent } from './home-news.component';
describe('HomeNewsComponent', () => {
  let component: HomeNewsComponent;
  let fixture: ComponentFixture<HomeNewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeNewsComponent],
      imports: [CoreModule],
      providers: [{ provide: StoryService, useClass: StoryMockServiceService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
