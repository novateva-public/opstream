import { Component, OnInit } from '@angular/core';
import { StoryService } from '@core/service/story.service';
import { Story } from '@shared/models/story/story.interface';
import { forkJoin, map, switchMap } from 'rxjs';

@Component({
  selector: 'app-home-news',
  templateUrl: './home-news.component.html',
  styleUrls: ['./home-news.component.sass'],
})
export class HomeNewsComponent implements OnInit {
  stories: Array<Story> = [];
  storiesFiltered: Array<Story> = [];
  constructor(private readonly storyService: StoryService) {}

  ngOnInit(): void {
    this.storyService
      .getTopStories()
      .pipe(
        map((ids) => {
          return ids.slice(0, 30);
        }),
        map((ids) => ids.map((id) => this.storyService.getStory(id))),
        switchMap((storiesData$) => forkJoin(...storiesData$))
      )
      .subscribe((stories) => {
        this.stories = stories;
        this.storiesFiltered = stories;
      });

    this.storyService.nameChange.subscribe((filter) => {
      this.storiesFiltered = this.stories.filter((story) =>
        story.title.toLowerCase().includes(filter.toLowerCase())
      );
    });
  }
}
