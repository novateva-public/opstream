import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StoryService } from '@core/service/story.service';
import { Story } from '@shared/models/story/story.interface';
import { forkJoin, map, switchMap } from 'rxjs';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.sass'],
})
export class NewComponent implements OnInit {
  story: Story = {} as Story;
  comments: Story[] = [];
  constructor(
    private route: ActivatedRoute,
    private storyService: StoryService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(({ id }) =>
      this.storyService
        .getStory(id)
        .pipe(
          map((story) => {
            this.story = story;
            return story.kids.slice(0, 5);
          }),
          map((ids) => ids.map((id) => this.storyService.getStory(id))),
          switchMap((storiesData$) => forkJoin(...storiesData$))
        )
        .subscribe((comments) => (this.comments = comments))
    );
  }
}
