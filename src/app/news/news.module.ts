import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeNewsComponent } from './pages/home-news/home-news.component';
import { NewComponent } from './pages/new/new.component';
import { RouterModule } from '@angular/router';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';
import { AppRoutes } from './news.router';

@NgModule({
  declarations: [HomeNewsComponent, NewComponent],
  imports: [RouterModule.forChild(AppRoutes), CoreModule, SharedModule],
})
export class NewsModule {}
