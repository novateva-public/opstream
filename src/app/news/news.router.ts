import { Routes } from '@angular/router';
import { HomeNewsComponent } from './pages/home-news/home-news.component';
import { NewComponent } from './pages/new/new.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: HomeNewsComponent,
  },
  {
    path: 'item/:id',
    component: NewComponent,
  },
];
