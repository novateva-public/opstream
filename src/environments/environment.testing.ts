import { StoryService } from '@core/service/story.service';
import { StoryMockServiceService } from '@core/mocks/story-mock-service.service';

export const environment = {
  production: true,
  providers: [{ provide: StoryService, useClass: StoryMockServiceService }],
};
